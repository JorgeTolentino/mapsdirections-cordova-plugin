package pe.codebox.cordova.mapsdirections;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

public class MapsDirections extends CordovaPlugin {

    /**
     * Constructor.
     */
    public MapsDirections() {
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArray of arguments for the plugin.
     * @param callbackContext   The callback context used when calling back into JavaScript.
     * @return                  True when the action was valid, false otherwise.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("open")) {
            String oLatitude =  args.getString(0);
            String oLongitude =  args.getString(1);
            String dLatitude =  args.getString(2);
            String dLongitude =  args.getString(3);
            this.open(callbackContext, oLatitude, oLongitude, dLatitude, dLongitude);
        }else{
            return false;
        }
        
        return true;
    }

    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------

    public void open(CallbackContext callbackContext, String oLatitude, String oLongitude, String dLatitude, String dLongitud){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
        Uri.parse("https://maps.google.com/maps?saddr="+oLatitude+","+oLongitude+"&daddr="+dLatitude+","+dLongitud));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        
        try
        {
            cordova.getActivity().startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            callbackContext.error("Please install Google Maps");
        }
    }
}
