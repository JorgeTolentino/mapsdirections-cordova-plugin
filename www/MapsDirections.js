var exec = require('cordova/exec');

module.exports = {
    open: function(params, error) {
        exec(null, error, "MapsDirections", "open", [params.oLatitude, params.oLongitude, params.dLatitude, params.dLongitude]);
    },
};
